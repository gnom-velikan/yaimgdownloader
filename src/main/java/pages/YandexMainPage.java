package pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;



public class YandexMainPage {

    public SelenideElement getHomeLogo() {
    return $(".home-logo");
}

    public SelenideElement getYandexImagesButton() {
    return $(By.xpath("//a[@data-id = 'images']"));
}

    public SelenideElement getTabs() {
        return $(".home-arrow__tabs");
    }

    public SelenideElement getSearchForm() {
        return $(".home-arrow__search form");
    }



public void goToImagesPage() {
    getYandexImagesButton().click();
}





}
