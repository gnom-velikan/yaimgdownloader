package pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static java.lang.Integer.parseInt;

public class YandexImagesPage {

    public SelenideElement getSearchInput() {
        return $(".search2__input input");
    }

    public SelenideElement getFilterButton() {
        return $(".under-menu__filters-toggle");
    }

    public SelenideElement getColorButton() {
        return $(".filter-color");
    }

    public SelenideElement getRedColorButton() {
        return $(".radiobox__radio_color_red");
    }

    public SelenideElement getTabs() {
        return $(".tabs-navigation");
    }

    public SelenideElement getLogo() {
        return $(".logo");
    }

    public SelenideElement getOneImage(int numberOfImage) {
        System.out.println("Выбрана картинка номер " + numberOfImage);

        return $(".serp-item_pos_" + numberOfImage).waitUntil(visible, 1000);

    }

    // Получение размера картинки
    public int getImageSize() {

       String imageSize = $(".button2__text.sizes__sizes").shouldBe(visible).innerText();

       System.out.println("Размер картинки = " + imageSize);

       String width = imageSize.substring(0, imageSize.indexOf("×"));

       System.out.println("Ширина картинки = " + width);

       String height = imageSize.substring(imageSize.indexOf("×") + 1, imageSize.length());

       int valueOfImageSize = parseInt(width) * parseInt(height);

       System.out.println("Финальный размер картинки = " + valueOfImageSize);

        $(By.xpath("//a[@aria-label = 'Закрыть']")).shouldBe(visible).click();

        return valueOfImageSize;

    }

    // Выбираем наибольшую картинку
    public int[] findBiggestImage(int numberOfImage) {

        int[] allImagesSizes = new int[numberOfImage];

        for (int i = 0; i < numberOfImage; i++) {

            getOneImage(i).click();
            allImagesSizes[i] = getImageSize();

            System.out.println("\n" + i + " Размер картинки = " + allImagesSizes[i] + "\n -----------");

        }

        return allImagesSizes;

    }

    // Находим индекс большей картинки
    public int findIndexOfBiggestImage(int[] array) {


        int indexOfMax = 0;
        int indexOfMin = 0;
        for (int i = 1; i < array.length; i++)
        {
            if (array[i] > array[indexOfMax])
            {
                indexOfMax = i;
            }
            else if (array[i] < array[indexOfMin])
            {
                indexOfMin = i;
            }
        }
        System.out.println("Самая большая картинка: " + indexOfMax + ", " + "Самая большая картинка: " + indexOfMin);

        return indexOfMax;

    }


}
