import com.codeborne.selenide.Configuration;
import org.junit.Test;
import pages.YandexImagesPage;
import pages.YandexMainPage;

import java.net.*;

import java.io.File;
import java.io.FileNotFoundException;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class DownloadImageTest {

    YandexMainPage yandexMainPage = new YandexMainPage();
    YandexImagesPage yandexImagesPage = new YandexImagesPage();


    @Test
    public void test() {
        Configuration.browser = "chrome";

        YandexMainPage yandexMainPage = new YandexMainPage();

        open("https://yandex.ru");

        // Проверяем, что отображается лого яндекса
        yandexMainPage.getHomeLogo().shouldBe(visible);

        // Проверяем, что отображаюся табы
        yandexMainPage.getTabs().shouldBe(visible);

        // Проверяем, что отображается форма поиска
        yandexMainPage.getSearchForm().shouldBe(visible);

        //На страничке Яндекса проверил что отображается того, табы и форма поиска.
        //Думаю этого достаточно, что-бы понять, что страничка загрузилась

        // Переход на страницу картинок
        yandexMainPage.goToImagesPage();

        //Не переходим сразу на урл картинок, а кликаем по табу, так мы проверяем и его работу


        YandexImagesPage yandexImagesPage = new YandexImagesPage();

        // Проверяем, что отображается лого яндекса
        yandexImagesPage.getLogo().shouldBe(visible);

        // Проверяем что отображается форма ввода
        yandexImagesPage.getSearchInput().shouldBe(visible);

        // Проверяем что отображаются табы
        yandexImagesPage.getTabs().shouldBe(visible);

        // Вводим поисковой запрос
        yandexImagesPage.getSearchInput().setValue("Самолет").pressEnter();

        // Выставляем фильтр по красному цвету
        yandexImagesPage.getFilterButton().shouldBe(visible).click();
        yandexImagesPage.getColorButton().click();
        yandexImagesPage.getRedColorButton().click();


        // Выбераем наибольшую картинку
        int bestImage = yandexImagesPage.findIndexOfBiggestImage(yandexImagesPage.findBiggestImage(7));

        // Находим наибольшую картинку и кликаем по ней
        yandexImagesPage.getOneImage(bestImage).click();

        // Скачиваем картинку и ставим название сайта в название файла
        try {

            String newName = $(".i-bem.sizes_js_inited.sizes_buttons-theme_action > a").getAttribute("href");
            System.out.println("Новое название1 " + newName);
            URL fileNewName = new URL(newName);
            String lastName = fileNewName.getHost();

            System.out.println("Название сайта " + lastName);

            File file1 = $(".i-bem.sizes_js_inited.sizes_buttons-theme_action > a").download();

            File file2 = new File(lastName);


            // Переименование
            if(file1.renameTo(file2)) {
                System.out.println("Rename succesful");
            }else{
                System.out.println("Rename failed");
            }


            System.out.println(file2.getAbsolutePath());
            System.out.println("Имя файла = " + file2.getName());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }




    }


}


